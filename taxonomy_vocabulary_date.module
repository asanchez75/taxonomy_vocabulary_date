<?php

/**
 * @file
 * Contains taxonomy_vocabulary_date.module..
 */

use \Drupal\Core\Form\FormStateInterface;
use \Drupal\taxonomy\Entity\Vocabulary;

/**
 * Implements hook_form_FORM_ID_alter().
 * @param $form
 * @param FormStateInterface $form_state
 * @param $form_id
 */
function taxonomy_vocabulary_date_form_alter(&$form, FormStateInterface $form_state, $form_id) {
  if ($form_id === 'taxonomy_vocabulary_form') {

    $vocabulary = $form_state->getFormObject()->getEntity();

    $options = array('Standard Taxonomy', 'Códigos CAD', 'Objetivos de Desarrollo Sostenible', 'Plan Director', 'Marco de Asociación');

    $form['type'] = [
      '#title' => t('Type'),
      '#type' => 'select',
      '#options' => array_combine($options, $options),
      '#default_value' => $vocabulary->getThirdPartySetting('taxonomy_vocabulary_date', 'type'),
    ];

    $form['start_date'] = [
      '#title' => t('Start date'),
      '#type' => 'date',
      '#attributes' => array('type'=> 'date', 'min'=> '-5 years', 'max' => '+5 years' ),
      '#date_date_format' => 'd/m/Y',
      '#default_value' => $vocabulary->getThirdPartySetting('taxonomy_vocabulary_date', 'start_date'),
    ];

    $form['end_date'] = [
      '#title' => t('End date'),
      '#type' => 'date',
      '#attributes' => array('type'=> 'date', 'min'=> '-5 years', 'max' => '+5 years' ),
      '#date_date_format' => 'd/m/Y',
      '#default_value' => $vocabulary->getThirdPartySetting('taxonomy_vocabulary_date', 'end_date'),
    ];

    $form['#attached']['library'][] = 'taxonomy_vocabulary_date/taxonomy_vocabulary_date_route';

    $form['#entity_builders'][] = 'taxonomy_vocabulary_date_form_builder';

  }
}

/**
 * Our custom entity builder
 * @param $entity_type
 * @param Vocabulary $vocabulary
 * @param $form
 * @param FormStateInterface $form_state
 */
function taxonomy_vocabulary_date_form_builder($entity_type, Vocabulary $vocabulary, &$form, FormStateInterface $form_state){

  if ($form_state->getValue('type')) {
    $vocabulary->setThirdPartySetting(
      'taxonomy_vocabulary_date',
      'type',
      $form_state->getValue('type')
    );
  }

  $query = \Drupal::entityQuery('taxonomy_vocabulary');
  $vids = $query->execute();
  $current_vocabulary = $vocabulary->get('name');
  foreach($vids as $vid) {
    // to optimize three times the vocabulary loading
    $name = Vocabulary::load($vid)->get('name');
    $start_date = Vocabulary::load($vid)->getThirdPartySetting('taxonomy_vocabulary_date', 'start_date');
    $end_date = Vocabulary::load($vid)->getThirdPartySetting('taxonomy_vocabulary_date', 'end_date');
    $type = Vocabulary::load($vid)->getThirdPartySetting('taxonomy_vocabulary_date', 'type');

    if ($type !== 'Standard Taxonomy') {
      $dates[$name]['start_date'] = $start_date;
      $dates[$name]['end_date'] = $end_date;
      $dates[$name]['name'] = $name;
    }

  }

  unset($dates[$current_vocabulary]);

  $start_date_input = $form_state->getValue('start_date');
  $end_date_input = $form_state->getValue('end_date');

  if ($start_date_input) {
    foreach ($dates as $key => $value) {
      if (( (strtotime($value['start_date']) <= strtotime($start_date_input)) &&
        (strtotime($start_date_input) <= strtotime($value['end_date']))
         )) {
        drupal_set_message(t('The given start date @start_date_input collides with time interval <@start_date, @end_date> configured in @taxonomy_name taxonomy',
          array('@start_date_input' => $start_date_input, '@start_date' => $value['start_date'], '@end_date' => $value['end_date'], '@taxonomy_name' => $value['name'])), 'error');
      }
    }
    $vocabulary->setThirdPartySetting(
      'taxonomy_vocabulary_date',
      'start_date',
      $start_date_input
    );
  } else{
    $vocabulary->unsetThirdPartySetting('taxonomy_vocabulary_date', 'start_date');
  }

  if ($end_date_input) {
    foreach ($dates as $key => $value) {
      if ((  (strtotime($end_date_input) <= strtotime($value['end_date'])) &&
        (strtotime($value['start_date']) <= strtotime($end_date_input))  )) {
        drupal_set_message(t('The given end date @end_date_input collides with time interval <@start_date, @end_date> configured in @taxonomy_name taxonomy',
          array('@end_date_input' => $end_date_input, '@start_date' => $value['start_date'], '@end_date' => $value['end_date'], '@taxonomy_name' => $value['name'])), 'error');
      }
    }
    $vocabulary->setThirdPartySetting(
      'taxonomy_vocabulary_date',
      'end_date',
      $end_date_input
    );
  } else{
    $vocabulary->unsetThirdPartySetting('taxonomy_vocabulary_date', 'end_date');
  }

}

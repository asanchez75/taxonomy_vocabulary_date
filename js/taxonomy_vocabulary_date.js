(function($){
  $(document).ready(function(){

    if ($('.taxonomy-vocabulary-form .form-select').val() === 'Standard Taxonomy') {
        $('.taxonomy-vocabulary-form .form-date').css('display', 'none');
        $('.taxonomy-vocabulary-form label[for=edit-start-date]').css('display', 'none');
        $('.taxonomy-vocabulary-form label[for=edit-end-date]').css('display', 'none');
    }

    $('.taxonomy-vocabulary-form .form-select').on('change', function() {
      if (this.value === 'Standard Taxonomy') {
        $('.taxonomy-vocabulary-form .form-date').css("display","none");
        $('.taxonomy-vocabulary-form label[for=edit-start-date]').css("display","none");
        $('.taxonomy-vocabulary-form label[for=edit-end-date]').css("display","none");
      }
      else {
        $('.taxonomy-vocabulary-form .form-date').css("display","block");
        $('.taxonomy-vocabulary-form label[for=edit-start-date]').css("display","block");
        $('.taxonomy-vocabulary-form label[for=edit-end-date]').css("display","block");
      }
    })
  })
})(jQuery)



